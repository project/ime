/**
 * @file
 * To add input method support to the editable fields of a web page.
 */

(function($) {
  Drupal.behaviors.jqueryImeDrupal = {
    attach: function(context, settings) {
      var flb_uid = Drupal.settings.ime.field_ids;
      var flb_path = Drupal.settings.ime.jqueryImePath;

      // Array to store Language Code and 1'st input method.
      var lang_script = [];
      $.each($.ime.languages, function(key, value) {
        var obj = {};
        obj[key] = value['inputmethods'][0];
        if (key != 'en') {
          lang_script.push(obj);
        }
      });
      $.each(lang_script, function(key, value) {
        $.extend($.ime.preferences['registry']['imes'], value);
      });
      $(flb_uid).ime({imePath: flb_path});
    }
  };
})(jQuery);
